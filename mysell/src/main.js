import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 引入elementui js/css文件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false

// 引入重置表
import '@/assets/reset.css'
// 引入图标库
import '@/assets/fonts/iconfont.css'

// 引入echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

// 挂载elementui
Vue.use(ElementUI);
 
// 挂载一个空实例对象作为中介传参
Vue.prototype.$bus = new Vue();

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
