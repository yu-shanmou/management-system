import req from '@/utilis/req.js'

// 添加分类
export function addType(params){
    return req.post("/goods/addcate",params)
}

// 获取分类
export function getType(params){
    return req.get("/goods/catelist",params)
}

// 刪除分类
export function delType(params){
    return req.get("/goods/delcate",params)
}

// 修改分类
export function eiditGoods(params){
    return req.post("/goods/editcate",params)
}

// 查询所有分类
export function cateType(params){
    return req.get("/goods/categories",params)
}

// 添加商品
export function addGoods(params){
    return req.post("/goods/add",params)
}

// 获取商品列表
export function getGoods(params){
    return req.get("/goods/list",params)
}

// 删除商品
export function delGoods(params){
    return req.get("/goods/del",params)
}

// 修改商品
export function editGoods(params){
    return req.post("/goods/edit",params)
}
