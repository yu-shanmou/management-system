import req from '@/utilis/req.js'

// 获取店铺详情
export function getShop(){
    return req.get("/shop/info")
}

// 店铺内容修改
export function shopEdit(params){
    return req.post("/shop/edit",params)
}
