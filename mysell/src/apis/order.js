// 订单功能接口
import req from '@/utilis/req.js'
// 获取订单列表
export function getOrderList(params){
    return req.get("/order/list",params)
}

// 查询订单
export function searchOrder(params){
    return req.get("/order/search",params)
}

// 首页报表接口
export function totalData(){
    return req.get("/order/totaldata")
}

// 订单报表接口
export function orderTotal(params){
    return req.get("/order/ordertotal",params)
}

// 获取订单详情
export function detail(params){
    return req.get("/order/detail",params)
}

// 获取订单详情
export function editOrder(params){
    return req.post("/order/edit",params)
}