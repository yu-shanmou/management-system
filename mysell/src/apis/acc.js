import req from '@/utilis/req.js'

// 登录接口层  api层
export function checkLogin(params){
    return req.post("/users/checkLogin",params)
}

// 添加账号
export function addAcc(params){
    return req.post("/users/add",params)
}

// 获取账号列表
export function getList(params){
    return req.get("/users/list",params)
}

// 删除账号
export function delAcc(params){
    return req.get("/users/del",params)
}

// 批量删除
export function delAccAll(params){
    return req.get("/users/batchdel",params)
}

// 修改账号
export function changgeAcc(params){
    return req.post("/users/edit",params)
}

// 检查旧密码
export function oldPwd(params){
    return req.get("/users/checkoldpwd",params)
}

// 修改密码
export function changgePwd(params){
    return req.post("/users/editpwd",params)
}

// 获取账号个人中心信息
export function getInfo(){
    return req.get("/users/info")
}

// 上传头像接口
export function avatarUp(params){
    return req.get("/users/avataredit",params)
}

// 获取用户角色
export function getUser(){
    return req.get("/users/role")
}

