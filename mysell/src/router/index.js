import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

// 解决 点击路由会重复报警告的问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

import Login from '@/views/Login'
import Error404 from '@/views/Error404'
import Layout from '@/views/layout/Layout'
import local from "@/utilis/local.js"
export const asyncRoutes = [


    // 账号管理
    {
        path: "/acc",
        component: Layout,
        redirect: "/acc/acc-list",
        meta: {
            title: "账号管理",
            role: ["super", "normal"]
        },
        children: [{
                path: "/acc/acc-list",
                meta: {
                    title: "账号列表",
                    role: ["super"]

                },
                component: () => import("@/views/acc/AccList.vue")
            },
            {
                path: "/acc/acc-add",
                meta: {
                    title: "账号添加",
                    role: ["super"]
                },
                component: () => import("@/views/acc/AccAdd.vue")
            },
            {
                path: "/acc/acc-eidit",
                meta: {
                    title: "修改密码",
                    role: ["super", "normal"]
                },
                component: () => import("@/views/acc/AccEidit.vue")
            },
            {
                path: "/acc/person",
                meta: {
                    title: "个人中心",
                    role: ["super", "normal"]
                },
                component: () => import("@/views/acc/Person.vue")
            }
        ],
    },
    // 销售统计 
    {
        path: "/count",
        component: Layout,
        meta: {
            title: "销售统计",
            role: ["super"]
        },
        redirect: "/goods-count",
        children: [{
                path: "/count/goods-count",
                meta: {
                    title: "商品统计"
                },
                component: () => import("@/views/count/GoodsCount.vue")
            },
            {
                path: "/count/order-count",
                meta: {
                    title: "订单统计"
                },
                component: () => import("@/views/count/OrderCount.vue")
            }
        ],
    },
    {
        path: "/404",
        component: () => import("@/views/Error404.vue"),
        hidden: true
    },
    {
        path: "*",
        redirect: "/404",
    }
]

const router = new VueRouter({
    routes: [
        // 登录
        {
            path: "/Login",
            component: Login
        },
        // 导航首页
        {
            path: "/",
            component: Layout,
            redirect: "/home",
            children: [{
                path: "home",
                component: () => import("@/views/home/Home.vue")
            }],
        },
        // 订单管理
        {
            path: "/order",
            component: Layout,
            meta: {
                title: "订单管理"
            },
            redirect: "/order/order-list",
            children: [{
                    meta: {
                        title: "订单列表"
                    },
                    path: "/order/order-list",
                    component: () => import("@/views/order/OrderList.vue")
                },
                {
                    path: "/order/order-info",
                    component: () => import("@/views/order/OrderInfo.vue")
                }
            ],
        },
        // 商品管理
        {
            path: "/goods",
            component: Layout,
            redirect: "/goods/goods-list",
            meta: {
                title: "商品管理"
            },
            children: [{
                    path: "/goods/goods-list",
                    meta: {
                        title: "商品列表"
                    },
                    component: () => import("@/views/goods/GoodsList.vue")
                },
                {
                    path: "/goods/goods-add",
                    meta: {
                        title: "商品添加"
                    },
                    component: () => import("@/views/goods/GoodsAdd.vue")
                },
                {
                    path: "/goods/goods-type",
                    meta: {
                        title: "商品分类"
                    },
                    component: () => import("@/views/goods/GoodsType.vue")
                }
            ],
        },
        // 店铺管理
        {
            path: "shop",
            component: Layout,
            redirect: "/shop",
            meta: {
                title: "店铺管理"
            },
            children: [{
                path: "/shop",
                component: () => import("@/views/shop/Shop.vue")
            }],
        },
    ]

})

// 路由守卫
router.beforeEach((to, from, next) => {
    if (to.path == "/login") {
        next();
    }
    // 通过本地存储判断用户是否有登录的token
    var islogin = local.getItem("tk") ? true : false;
    if (islogin) {
        next()
    } else {
        next("/login")
    }
})

export default router