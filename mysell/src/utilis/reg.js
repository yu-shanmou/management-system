// 账号验证
export const ACC_REG = /^[\u4e00-\u9fa5_a-zA-Z0-9_-]{2,15}$/;

// 密码验证
export const PWD_REG = /^[a-zA-Z0-9_-]{3,15}$/;

// 商品添加
export const GOODS_REG = /^[\u4e00-\u9fa5_a-zA-Z0-9_-]{2,9}$/;