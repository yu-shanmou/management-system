// 本地存储封装
export default {

    // 存
    setItem(key, val){
        return localStorage.setItem(key, val)
    },

    // 取
    getItem(key){
        return localStorage.getItem(key)
    },

    // 删除
    remove(key){
        return localStorage.removeItem(key)
    },

    // 清除
    clear(){
        return localStorage.clear()
    }
}