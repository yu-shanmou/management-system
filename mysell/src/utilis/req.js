// axios 封装
import axios from 'axios'
import qs from 'qs'
import local from '@/utilis/local'
// 引入路由
import router from "@/router"
import {
    Message
} from "element-ui"

// 设置 请求默认服务器地址
axios.defaults.baseURL = 'http://127.0.0.1:5000';

// 添加请求拦截器
axios.interceptors.request.use(config => {
    // 获取本地存储的“秘钥”
    let tk = local.getItem("tk");
    if (tk) {
        config.headers.Authorization = tk;
    }
    return config
})


// 添加响应拦截器
axios.interceptors.response.use((res) => {
    if (res.data.code != undefined && res.data.msg != undefined) {
        let {
            code,
            msg
        } = res.data;
        // 判断code==401  说明token错误 去首页
        if (code !== 401) {
            if (code == 0) {
                Message({
                    message: msg,
                    type: "success"
                })
            } else {
                Message.error(msg)
            }
        }
    } 
    return res
},(error=>{
    if(error.response.data.code == 401){
            router.push("/login")
    }
}))


export default {
    // 封装get请求
    get(url, params = {}) {
        return new Promise((reslove, reject) => {
            axios.get(url, {
                params
            }).then(response => {
                reslove(response.data)
            }).catch(err => {
                reject(err)
            })
        })
    },

    // 封装post请求

    post(url, params = {}) {
        return new Promise((reslove, reject) => {
            axios.post(url, qs.stringify(params)).then(response => {
                reslove(response.data)
            }).catch(error => {
                reject(error)
            })
        })
    }
}