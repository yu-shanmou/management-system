// 引入
import Vue from 'vue';
import Vuex from 'vuex';

// 注册
Vue.use(Vuex);

// 实例化仓储结构
const store = new Vuex.Store({
  state: { // 源数据管理
    shopInfo: {}, // 商家信息
    allGoods: [], // 初始数据
    choosedGoods: [], // 初始购物车已选数据
    currentGoods: {}, // 当前商品详情
    allAssess: [], // 评价数据
  },
  getters: { // 计算数据
    newAllGoods: state => { // 计算商品信息
      return state.allGoods;
    },
  },
  mutations: { // 同步修改源数据的方法
    // 初始商家信息
    initShopInfo(state, payLoad) {
      state.shopInfo = payLoad;
    },
    // 初始商品信息
    initAllGoods(state, payLoad) {
      state.allGoods = payLoad.map(item => {
        item.foods.forEach(_item => {
          _item.goodsCount = 0
        })
        return item
      });
    },
    // 更新 商品与购物车信息
    updateGoodsCount(state, payLoad) {
      state.allGoods.forEach(item => {
        item.foods.forEach(_item => {
          if (_item.id === payLoad.id) {
            if (payLoad.type == 'add') {
              _item.goodsCount++;
              dealChoosed(state, _item);
            } else {
              _item.goodsCount = _item.goodsCount > 0 ? --_item.goodsCount : 0;
              dealChoosed(state, _item);
            }
          }
        })
      })
    },
    // 获取当前商品详情
    getCurrentGoods(state, payLoad) {
      state.allGoods.forEach((item) => {
        item.foods.forEach((_item) => {
          if(_item.id == payLoad) {
            state.currentGoods = _item
          }
        })
      })
    },
    // 初始评价数据
    initAllAssess(state, payLoad) {
      state.allAssess = payLoad;
    },
  },
  actions: {}, // 异步修改源数据的方法
  modules: {} // 模块(每一个模块都包含完整的state, getters, mutations, actions, modules)
});

// 处理购物车与商品面板的交互
function dealChoosed(state, item) {
  let flag = true; // 用于判断是否添加商品进购物车，true为加 false不加
  // 遍历购物车数据
  state.choosedGoods.forEach(_item => {
    // 进判断: 购物车一定含有与当前id对应的商品(即数量大于1),只需赋值
    if (_item.id === item.id) {
      _item.goodsCount = item.goodsCount;
      flag = false;
    }
  })
  // 进判断: 即购物车无当前操作的商品,因此追加
  if (flag) {
    state.choosedGoods.unshift(item);
  }
  // 进判断: 即商品数量从0 -- 1时，删除购物车对应的商品数据
  if (item.goodsCount === 0) {
    state.choosedGoods.forEach((_item, index) => {
      if (_item.id === item.id) {
        state.choosedGoods.splice(index, 1);
      }
    })
  }
}

// 暴露
export default store;