import Vue from 'vue'
import App from './App.vue'

// 引入Vant
import Vant from 'vant';
import 'vant/lib/index.css';

// 引入路由
import router from '@/router';

// 引入store
import store from '@/store';

// 引入重置样式
import '@/assets/css/reset.css';

Vue.use(Vant);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router, // 挂载路由
  store // 挂载仓储中心
}).$mount('#app')
