// 引入
import Vue from 'vue';
import VueRouter from 'vue-router';

import Index from '@/pages/Index/Index'; // 首页

// 注册
Vue.use(VueRouter);

// 结构
const routes = [
  {
    path: '/',
    component: Index
  },
  {
    path: '/index',
    redirect: '/'
  },
  {
    path: '/detail',
    component: () => import('@/pages/Detail/Detail') // 详情页
  }
];

// 实例
const router = new VueRouter({
  routes
})

// 暴露
export default router;