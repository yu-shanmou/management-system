// 引入axios.js
import axios from '@/apis/axios';

/**
 * 获取评价数据
 */

export function getRatings_api() {
  return axios.get('/shop/ratings');
}
