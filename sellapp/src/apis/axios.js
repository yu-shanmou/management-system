import axios from 'axios'
import qs from 'qs'
//请求拦截 interceptors
axios.interceptors.request.use(
    // 请求拦截器中，config可以获取全部的请求配置（methods，headers，baseURL，data）
    config => {
        if (
            config.method === 'post' ||
            config.method === 'put' 
        ) {
            config.data = qs.stringify(config.data)
        }
        // else if (config.method === 'get') {
        //   config.headers = { 'token': 'abc' }
        // }
        const token = sessionStorage.getItem('token')
        config.headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            token: token || ''
        }
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

//返回拦截
axios.interceptors.response.use(
    // respones能获取到返回信息全部配置（status，headers，data）
    response => {
        if (response.status === 200) {
            return response.data
        } else {
            return new Error(response)
        }
    },
    error => {
        return new Promise.reject(error)
    }
)

// baseURL：设置axios请求的根路径
// 一旦设置根路径，比如是：http:www.itsource.cn以后，所有请求都会被加上这个路径
// 比如，请求/goods/goods_list,就会变成：http://www.itsource.cn/goods/goods_list
// 此处如果设置baseURL是“/api”，那么上面的请求就变成了：/api/goods/goods_list
// 实际上/api并非参与请求，而是跨域地址匹配符。
// 所有接口只要以/api开头，脚手架就会将/api替换成我们设置的地址，然后由代理去帮忙请求再返回回来
axios.defaults.baseURL = '/api'

export default axios
