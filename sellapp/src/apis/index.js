// 引入axios.js
import axios from '@/apis/axios';

/**
 * 获取商品数据
 */

 export function getGoodsList_api() {
   return axios.get('/goods/goods_list');
 }

 /**
  * 获取商家
  */
 export function getSeller_api() {
  return axios.get('/shop/seller');
}
