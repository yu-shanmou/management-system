module.exports = {
  devServer: {
      // 此处设置proxy代理以后，在代理开发阶段，真正发请求的并非是axios，而是本地localhost:8080服务，请求拿到数据以后，返回给axios，axios返回到页面
      proxy: {
          // 此处的api代表地址匹配符（当接口请求以/api开头的时候，就立即将/api替换成target中的目标地址）
          '/api': {
              target: 'http://localhost:5000',
              changeOrigin: true,
              ws: true,
              pathRewrite: {
                  '^/api': ''
              }
          }
      }
  }
}